package com.xogoda.minecraftskins.ads;

public class AdsObj {

    private String ADS_PRIORITY_NO_1;
    private String ADS_PRIORITY_NO_2;
    private String ADS_ADMOB_PUBID;
    private String ADS_ADMOB_BANNER;
    private String ADS_ADMOB_INTERSTITIALS;
    private int ADS_ADMOB_INTERSTITIALS_CLICK;
    private String ADS_STARTAPP_APPID;
    private String currentActivatedAds;

    public String getADS_PRIORITY_NO_1() {
        return ADS_PRIORITY_NO_1;
    }

    public void setADS_PRIORITY_NO_1(String ADS_PRIORITY_NO_1) {
        this.ADS_PRIORITY_NO_1 = ADS_PRIORITY_NO_1;
    }

    public String getADS_PRIORITY_NO_2() {
        return ADS_PRIORITY_NO_2;
    }

    public void setADS_PRIORITY_NO_2(String ADS_PRIORITY_NO_2) {
        this.ADS_PRIORITY_NO_2 = ADS_PRIORITY_NO_2;
    }

    public String getADS_ADMOB_PUBID() {
        return ADS_ADMOB_PUBID;
    }

    public void setADS_ADMOB_PUBID(String ADS_ADMOB_PUBID) {
        this.ADS_ADMOB_PUBID = ADS_ADMOB_PUBID;
    }

    public String getADS_ADMOB_BANNER() {
        return ADS_ADMOB_BANNER;
    }

    public void setADS_ADMOB_BANNER(String ADS_ADMOB_BANNER) {
        this.ADS_ADMOB_BANNER = ADS_ADMOB_BANNER;
    }

    public String getADS_ADMOB_INTERSTITIALS() {
        return ADS_ADMOB_INTERSTITIALS;
    }

    public void setADS_ADMOB_INTERSTITIALS(String ADS_ADMOB_INTERSTITIALS) {
        this.ADS_ADMOB_INTERSTITIALS = ADS_ADMOB_INTERSTITIALS;
    }

    public int getADS_ADMOB_INTERSTITIALS_CLICK() {
        return ADS_ADMOB_INTERSTITIALS_CLICK;
    }

    public void setADS_ADMOB_INTERSTITIALS_CLICK(int ADS_ADMOB_INTERSTITIALS_CLICK) {
        this.ADS_ADMOB_INTERSTITIALS_CLICK = ADS_ADMOB_INTERSTITIALS_CLICK;
    }

    public String getADS_STARTAPP_APPID() {
        return ADS_STARTAPP_APPID;
    }

    public void setADS_STARTAPP_APPID(String ADS_STARTAPP_APPID) {
        this.ADS_STARTAPP_APPID = ADS_STARTAPP_APPID;
    }

    public String getCurrentActivatedAds() {
        return currentActivatedAds;
    }

    public void setCurrentActivatedAds(String currentActivatedAds) {
        this.currentActivatedAds = currentActivatedAds;
    }
}
