package com.xogoda.minecraftskins.ads;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.xogoda.minecraftskins.api.ApiClient;
import com.xogoda.minecraftskins.api.ApiInterface;
import com.xogoda.minecraftskins.utils.Utilities;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.startapp.android.publish.ads.banner.Banner;
import com.startapp.android.publish.adsCommon.StartAppSDK;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdsAssistants {

    private Activity AdsAssist;

    private String AdsAdmobPubId, AdsAdmobBanner, AdsAdmobInterstitals;
    private String AdsStartAppAppId;
    private String AdsPriorityNo1, AdsPriorityNo2;
    private int AdsMerge, AdsAdmobInterstitalsClick;
    private AdsLoadListener adsLoadListener;

    private String activatedAds;
    private int counterPriority;

    private String MY_PREF_NAME = "ads-session";
    private String MY_PREF_KEY_STARTAPP_STARTED = "startapp_key";
    private String MY_PREF_KEY_ADSOBJ = "adsobj_key";

    private SharedPreferences adsSession;
    private AdsObj adsObj;
    private boolean failedLoadOnlineConfig;

    public AdsAssistants(Activity AdsAssist) {
        this.AdsAssist = AdsAssist;
        counterPriority = 1;

        adsSession = this.AdsAssist.getSharedPreferences(MY_PREF_NAME, Context.MODE_PRIVATE);
        adsObj = new AdsObj();
    }

    public boolean isFailedLoadOnlineConfig() {
        return failedLoadOnlineConfig;
    }

    public int getAdsMerge() {
        return AdsMerge;
    }

    private void checkAvailableAds() {
        Log.e("YVW", "Check Available Ads");
        //check priority no.1
        if (counterPriority == 1) {
            if (AdsPriorityNo1.equals(ApiClient.ADS_ADMOB)) {
                checkAdmobRequest();
            } else {
                runStartApp(true);

                if (adsLoadListener != null) {
                    adsLoadListener.onConfigLoaded();
                }
            }
        }
        if (counterPriority == 2) {
            if (AdsPriorityNo2.equals(ApiClient.ADS_ADMOB)) {
                checkAdmobRequest();
            } else {
                runStartApp(true);

                if (adsLoadListener != null) {
                    adsLoadListener.onConfigLoaded();
                }
            }
        }
    }

    public void runStartApp(boolean isFromCheckPart) {
        if (isFromCheckPart) {
            startStartAppSDK();
            saveAdsObj();
            saveSessionStartAppStatus(true);
        } else {
            //check is started
            boolean isStarted = isStartAppStarted(AdsAssist.getBaseContext());
            if (!isStarted) {
                startStartAppSDK();
            }
        }
    }

    private void startStartAppSDK() {
        StartAppSDK.init(AdsAssist, AdsStartAppAppId, true);
        //StartAppAd.disableSplash();
        activatedAds = ApiClient.ADS_STARTAPP;
    }


    private void checkAdmobRequest() {
        //Try to request Ads Banner
        Log.e("YVW", "Check Admob Request");

        AdView adView = new AdView(AdsAssist);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(AdsAdmobBanner);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                //run next priority
                Log.e("YVW", "AdmobView failed to load");
                counterPriority++;
                checkAvailableAds();
            }

            @Override
            public void onAdLoaded() {
                Log.e("YVW", "Admob Request Loaded");
                activatedAds = ApiClient.ADS_ADMOB;
                saveAdsObj();
                if (adsLoadListener != null) {
                    adsLoadListener.onConfigLoaded();
                }
            }
        });
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    public void createAdmobBanner(LinearLayout linearAds, String admobBannerId) {
        AdView adView = new AdView(AdsAssist);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(admobBannerId);

        // Initiate a generic request to load it with an ad
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        // Place the ad view.
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        linearAds.addView(adView, params);
    }

    public void createStartAppBanner(LinearLayout startAppLayout) {
        // Define StartApp Banner
        Banner startAppBanner = new Banner(AdsAssist);
        LinearLayout.LayoutParams bannerParameters =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        // Add to main Layout
        startAppLayout.setGravity(Gravity.CENTER);
        startAppLayout.addView(startAppBanner, bannerParameters);
    }

    public void createStartAppBanner(RelativeLayout startAppLayout) {
        // Define StartApp Banner
        Banner startAppBanner = new Banner(AdsAssist);
        RelativeLayout.LayoutParams bannerParameters =
                new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        bannerParameters.addRule(RelativeLayout.CENTER_HORIZONTAL);
        bannerParameters.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        // Add to main Layout
        startAppLayout.addView(startAppBanner, bannerParameters);
    }

    public String getActivatedAds() {
        return activatedAds;
    }

    //    public void showSplashAdsStartApp(){
//        if(AdsMerge == 1 || Ads){
//
//        }
//
//        StartAppAd.showSplash(this, savedInstanceState,
//                new SplashConfig()
//                        .setTheme(SplashConfig.Theme.OCEAN)
//                        .setAppName(getString(R.string.app_name))
//                        .setLogo(R.mipmap.ic_launcher)   // resource ID
//                        .setOrientation(SplashConfig.Orientation.PORTRAIT)
//        );
//    }

    public void setAdsLoadListener(AdsLoadListener adsLoadListener) {
        this.adsLoadListener = adsLoadListener;
    }

    public void prepareAdsConfig() {
        Log.e("YVW", "Prepare Ads Config");
        if (ApiClient.ADS_CONFIGMODE == 1) {
            //Read Config Online
            if (Utilities.isConnected(AdsAssist)) {
                Log.e("YVW", "Run AdsOnline Task");
                getAdsConfig();
            } else {
                failedLoadOnlineConfig = true;
                adsLoadListener.onConfigLoaded();
            }
        } else {
            //Read Config Offline -- From AppConfig.java

            //Unit ID
            AdsAdmobPubId = ApiClient.ADS_ADMOB_PUBID;
            AdsAdmobBanner = ApiClient.ADS_ADMOB_BANNER;
            AdsAdmobInterstitals = ApiClient.ADS_ADMOB_INTERSTITIALS;
            AdsAdmobInterstitalsClick = ApiClient.ADS_ADMOB_INTERSTITIALS_CLICK;

            //StartApp ID
            AdsStartAppAppId = ApiClient.ADS_STARTAPP_APPID;

            //Priority Configuration
            AdsPriorityNo1 = ApiClient.ADS_PRIORITY_NO_1;
            AdsPriorityNo2 = ApiClient.ADS_PRIORITY_NO_2;

            //Merge ADS between Admob and StartApp
            AdsMerge = ApiClient.ADS_MERGE;

            checkAvailableAds();
        }
    }

    public void showToast(String msg) {
        Toast.makeText(AdsAssist, msg, Toast.LENGTH_LONG).show();
    }

    private void saveAdsObj() {
        adsObj = new AdsObj();
        adsObj.setADS_PRIORITY_NO_1(AdsPriorityNo1);
        adsObj.setADS_PRIORITY_NO_2(AdsPriorityNo2);
        adsObj.setADS_ADMOB_PUBID(AdsAdmobPubId);
        adsObj.setADS_ADMOB_BANNER(AdsAdmobBanner);
        adsObj.setADS_ADMOB_INTERSTITIALS(AdsAdmobInterstitals);
        adsObj.setADS_ADMOB_INTERSTITIALS_CLICK(AdsAdmobInterstitalsClick);
        adsObj.setADS_STARTAPP_APPID(AdsStartAppAppId);
        adsObj.setCurrentActivatedAds(activatedAds);

        Gson gson = new Gson();
        String adsObjJsonStr = gson.toJson(adsObj);

        SharedPreferences.Editor myEditor = adsSession.edit();
        myEditor.putString(MY_PREF_KEY_ADSOBJ, adsObjJsonStr);
        myEditor.commit();
    }

    public AdsObj getSessionAdsObj() {
        String adsObjJsonStr = adsSession.getString(MY_PREF_KEY_ADSOBJ, null);
        if (adsObjJsonStr != null) {
            Gson gson = new Gson();
            AdsObj adsObj = gson.fromJson(adsObjJsonStr, AdsObj.class);
            return adsObj;
        }

        return null;
    }

    public void resetSession() {
        Log.e("YVW", "Reset Session");
        SharedPreferences.Editor myEditor = adsSession.edit();
        myEditor.putBoolean(MY_PREF_KEY_STARTAPP_STARTED, false);
        myEditor.putString(MY_PREF_KEY_ADSOBJ, null);
        myEditor.commit();
    }

    private void saveSessionStartAppStatus(boolean started) {
        SharedPreferences.Editor myEditor = adsSession.edit();
        myEditor.putBoolean(MY_PREF_KEY_STARTAPP_STARTED, started);
        myEditor.commit();
    }

    private boolean isStartAppStarted(Context context) {
        return adsSession.getBoolean(MY_PREF_KEY_STARTAPP_STARTED, false);
    }

    private void getAdsConfig() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getSettings();
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    try {
                        JSONObject jObj = new JSONObject(response.body().string());

                        //Unit ID
                        AdsAdmobPubId = jObj.getString("admob_id");
                        AdsAdmobBanner = jObj.getString("banner_ad_id");
                        AdsAdmobInterstitals = jObj.getString("interstital_ad_id");
                        AdsAdmobInterstitalsClick = jObj.getInt("interstital_ad_click");

                        //StartApp ID
                        AdsStartAppAppId = jObj.getString("startapp_id");

                        //Priority Configuration
                        AdsPriorityNo1 = jObj.getString("ads_priority_1");
                        AdsPriorityNo2 = jObj.getString("ads_priority_2");

                        //Merge ADS between Admob and StartApp
                        AdsMerge = jObj.getInt("ADS_MERGE");

                        checkAvailableAds();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    showToast("Error Load Ads: Server Connection Error");
                    failedLoadOnlineConfig = true;
                    adsLoadListener.onConfigLoaded();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("AdsAssist", t.getMessage());
            }
        });
    }
}
