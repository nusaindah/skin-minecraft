package com.xogoda.minecraftskins.utils;

/**
 * Created by sawallubis on 10/14/16.
 */

public interface ActionClickItem {
    public void runClick();
}
