package com.xogoda.minecraftskins.utils;

/**
 * Created by KUNCORO on 25/07/2017.
 */

public interface PaginationAdapterCallback {
    void retryPageLoad();
}
