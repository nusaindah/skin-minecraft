package com.xogoda.minecraftskins.api;

import com.xogoda.minecraftskins.model.SkinResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by KUNCORO on 16/10/2018.
 */
public interface ApiInterface {
    @GET("api.php?latest&")
    Call<SkinResponse> getSkins(@Query("currentpage") int pageIndex);

    @GET("api.php")
    Call<SkinResponse> getSearch(@Query("search") String search,
                                @Query("currentpage") int pageIndex);

    @GET("api.php")
    Call<ResponseBody> getSettings();

}
