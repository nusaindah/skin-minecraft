package com.xogoda.minecraftskins.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by KUNCORO on 16/10/2018.
 */
public class ApiClient {

    public static final String URL = "https://minecraft.newfile.website/";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public final static String DIALOG_PREFERENCES = "dialog_preferences";
    public final static String DIALOG_STATUS = "dialog_status";

    //Don't change these    two lines of code
    public final static String ADS_ADMOB = "ADMOB";
    public final static String ADS_STARTAPP = "STARTAPP";

    //Online ADS
    //Value 1 = Mode Online -->
    //          - Configurable ADS Config and you must have JSON File on your private server or hosting
    //          - Put the URL for your JSON File to ADS_CONFIGMODE_ONLINE_URL value.
    //Value 0 = Mode Offline --> All config will using this file AdsConfig.java
    public static final int ADS_CONFIGMODE = 1;

    //Priority Configuration
    public static final String ADS_PRIORITY_NO_1 = ADS_STARTAPP;     //Main ADS
    public static final String ADS_PRIORITY_NO_2 = ADS_ADMOB;  //Back Up ADS

    //Unit ID
    public static final String ADS_ADMOB_PUBID = "";
    public static final String ADS_ADMOB_BANNER = "";
    public static final String ADS_ADMOB_INTERSTITIALS = "";
    public static final int ADS_ADMOB_INTERSTITIALS_CLICK = 1;

    //StartApp ID
    public static final String ADS_STARTAPP_APPID = "";

    //Merge ADS between Admob and StartApp
    //Value 1 = If you want to have two active ads (Admob and StartApp) in the same time.
    //          StartApp will appear in splash screen with SplashAds type.
    //Value 0 = If you only want to have one active ads, and antoher ads only for backup.
    public static final int ADS_MERGE = 1;


    //Configuration For OneSignal
    //Value true = Enable
    //Value false  = Disable
    public static final boolean ONESIGNAL_CONFIG = false;

}