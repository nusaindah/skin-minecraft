package com.xogoda.minecraftskins.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.xogoda.minecraftskins.R;
import com.xogoda.minecraftskins.model.SkinData;
import com.squareup.picasso.Picasso;
import com.xogoda.minecraftskins.ui.DetailSkin;
import com.xogoda.minecraftskins.utils.PaginationAdapterCallback;

import java.util.ArrayList;
import java.util.List;

public class SkinAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SkinData> skinData = new ArrayList<>();
    private Context context;

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

    private PaginationAdapterCallback mCallback;

    private String errorMsg;

    public SkinAdapter(Context context) {
        this.context = context;
//        this.mCallback = (PaginationAdapterCallback) context;
    }

    public List<SkinData> getSkinData() {
        return skinData;
    }

    public void setSkinData(List<SkinData> skinData) {
        this.skinData = skinData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View v1 = inflater.inflate(R.layout.row_skin, parent, false);
                viewHolder = new SkinVH(v1);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case ITEM:
                final SkinVH skinVH = (SkinVH) holder;

                Picasso.get()
                        .load(skinData.get(position).getThumbnail())
                        .resize(250, 250)
                        .centerCrop()
                        .into(skinVH.image);

                skinVH.title.setText(skinData.get(position).getTitle());

                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    Toast.makeText(context, context.getString(R.string.error_msg_unknown), Toast.LENGTH_SHORT).show();

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }

                break;
        }
    }

    @Override
    public int getItemCount() {
        return skinData == null ? 0 : skinData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == skinData.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private SkinAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final SkinAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildAdapterPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

     /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(SkinData r) {
        skinData.add(r);
        notifyItemInserted(skinData.size() - 1);
    }

    public void addAll(List<SkinData> skinData) {
        for (SkinData result : skinData) {
            add(result);
        }
    }

    public void remove(SkinData r) {
        int position = skinData.indexOf(r);
        if (position > -1) {
            skinData.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new SkinData());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = skinData.size() - 1;
        SkinData result = getItem(position);

        if (result != null) {
            skinData.remove(position);
            notifyItemRemoved(position);
        }
    }

    public SkinData getItem(int position) {
        return skinData.get(position);
    }

    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param show
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(skinData.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    /**
     * Main list's content ViewHolder
     */

    public class SkinVH extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView title;

        public SkinVH(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), DetailSkin.class);
                    intent.putExtra("thumbnail", skinData.get(getLayoutPosition()).getThumbnail());
                    intent.putExtra("url", skinData.get(getLayoutPosition()).getUrl());
                    intent.putExtra("title", skinData.get(getLayoutPosition()).getTitle());
                    intent.putExtra("id", skinData.get(getLayoutPosition()).getId());
                    v.getContext().startActivity(intent);
                }
            });
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }

}
