package com.xogoda.minecraftskins.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;
import com.xogoda.minecraftskins.R;
import com.xogoda.minecraftskins.adapter.SkinAdapter;
import com.xogoda.minecraftskins.ads.AdsAssistants;
import com.xogoda.minecraftskins.ads.AdsObj;
import com.xogoda.minecraftskins.api.ApiClient;
import com.xogoda.minecraftskins.api.ApiInterface;
import com.xogoda.minecraftskins.model.SkinData;
import com.xogoda.minecraftskins.model.SkinResponse;
import com.xogoda.minecraftskins.utils.ActionClickItem;
import com.xogoda.minecraftskins.utils.PaginationAdapterCallback;
import com.xogoda.minecraftskins.utils.PaginationScrollListener;
import com.xogoda.minecraftskins.utils.Utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Search extends Fragment implements PaginationAdapterCallback, SearchView.OnQueryTextListener {

    public Search() {
    }

    View rootView;

    GridLayoutManager gridLayoutManager;
    RecyclerView recycler;
    private ApiInterface apiInterface;
    List<SkinData> dataList = new ArrayList<SkinData>();
    SkinAdapter skinAdapter;
    String query;

    // Ads Online
    private ActionClickItem actionClickItem;
    private InterstitialAd mInterstitialAd;
    private LinearLayout linearAds;
    ProgressBar progressBar;
    LinearLayout errorLayout;
    Button btnRetry;
    TextView txtError;

    private AdsAssistants adsAssistants;
    private AdsObj adsObj;
    private StartAppAd startAppAd;
    private boolean runClickExecuted;

    private int mCounter = 0;
    int value_dialog = 0;
    SharedPreferences sharedpreferences;

    private int PAGE_START = 1;

    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES;
    private int currentPage = PAGE_START;

    private static final String TAG = Skin.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rootView = inflater.inflate(R.layout.recycler_search, container, false);
        getActivity().setTitle(R.string.menu_search);

        sharedpreferences = getActivity().getSharedPreferences(ApiClient.DIALOG_PREFERENCES, Context.MODE_PRIVATE);
        value_dialog = sharedpreferences.getInt(ApiClient.DIALOG_STATUS, 0);

        errorLayout = (LinearLayout) rootView.findViewById(R.id.error_layout);
        txtError = (TextView) rootView.findViewById(R.id.error_txt_cause);
        recycler = (RecyclerView) rootView.findViewById(R.id.recycler);

        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recycler.setLayoutManager(gridLayoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        skinAdapter = new SkinAdapter(getActivity());
        recycler.setAdapter(skinAdapter);

        linearAds = (LinearLayout) rootView.findViewById(R.id.ad_linear);

        adsAssistants = new AdsAssistants(getActivity());
        adsObj = adsAssistants.getSessionAdsObj();
        //generateInterstitialAd();

        if (adsObj != null) {
            if (adsObj.getCurrentActivatedAds().equals(ApiClient.ADS_ADMOB)) {
                linearAds.setVisibility(View.VISIBLE);

                adsAssistants.createAdmobBanner(linearAds, adsObj.getADS_ADMOB_BANNER());
            } else {
                startAppAd = new StartAppAd(getActivity());

                LinearLayout linearAds = (LinearLayout) rootView.findViewById(R.id.ad_linear);
                linearAds.setVisibility(View.VISIBLE);

                adsAssistants.createStartAppBanner(linearAds);
            }
        }

        if (value_dialog == 0) {
            dialogPrivacy();
        }

        recycler.addOnItemTouchListener(new SkinAdapter.RecyclerTouchListener(getActivity(),
                        recycler, new SkinAdapter.ClickListener() {

                    @Override
                    public void onClick(View view, final int position) {
                        mCounter++;
                        if (adsObj.getADS_ADMOB_INTERSTITIALS_CLICK() == mCounter) {
                            actionItemClicked(new ActionClickItem() {
                                @Override
                                public void runClick() {
                                }
                            });
                            mCounter = 0;
                        }
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                })
        );

        recycler.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        return rootView;
    }

    @Override
    public void retryPageLoad() {
        callData();
    }

    private void callData() {
        if (Utilities.isConnected(getActivity())) {
            loadFirstPage(query);
        } else {
            Toast.makeText(getActivity(), R.string.error_msg_timeout, Toast.LENGTH_SHORT).show();
        }
    }

    private void loadFirstPage(String query) {
        Log.e(TAG, "loadFirstPage: ");

        // To ensure list is visible when retry button in error view is clicked
        hideErrorView();

        callSkinResponse(query).enqueue(new Callback<SkinResponse>() {
            @Override
            public void onResponse(Call<SkinResponse> call, Response<SkinResponse> response) {
                TOTAL_PAGES = response.body().getTotalPages();

                hideErrorView();

                dataList = fetchResults(response);
                skinAdapter.addAll(dataList);

                if (currentPage <= TOTAL_PAGES) skinAdapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<SkinResponse> call, Throwable t) {
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    /**
     * @param response extracts List<{@link SkinData>} from response
     * @return
     */
    private List<SkinData> fetchResults(Response<SkinResponse> response) {
        SkinResponse skinResponse = response.body();
        return skinResponse.getResults();
    }

    private void loadNextPage() {
        Log.e(TAG, "loadNextPage: " + currentPage);

        callSkinResponse(query).enqueue(new Callback<SkinResponse>() {
            @Override
            public void onResponse(Call<SkinResponse> call, Response<SkinResponse> response) {
                skinAdapter.removeLoadingFooter();
                isLoading = false;
                TOTAL_PAGES = response.body().getTotalPages();

                dataList = fetchResults(response);
                skinAdapter.addAll(dataList);

                if (currentPage != TOTAL_PAGES) skinAdapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<SkinResponse> call, Throwable t) {
                t.printStackTrace();
                skinAdapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }

    /**
     * Performs a Retrofit call to the top rated movies API.
     * Same API call for Pagination.
     * As {@link #currentPage} will be incremented automatically
     * by @{@link PaginationScrollListener} to load next page.
     */
    private Call<SkinResponse> callSkinResponse(String query) {
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        return apiInterface.getSearch(query.replace(" ", "%20"), currentPage);
    }

//    @Override
//    public void retryPageLoad() {
//        loadNextPage();
//    }

    /**
     * @param throwable required for {@link #fetchErrorMessage(Throwable)}
     * @return
     */
    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);

            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!Utilities.isConnected(getActivity())) {
            errorMsg = getResources().getString(R.string.no_internet_connection);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    // Helpers -------------------------------------------------------------------------------------
    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
        }
    }

    private void dialogPrivacy() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater li = getLayoutInflater();
        View dialogView = null;
        dialogView = li.inflate(R.layout.dialog_privacy, null);
        builder.setView(dialogView);
        builder.setCancelable(false);
        builder.setTitle(R.string.dialog_title);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt(ApiClient.DIALOG_STATUS, 1);
                editor.apply();

                dialog.dismiss();
            }
        });

        builder.show();
    }

    public void actionItemClicked(ActionClickItem actionClickItemParam) {
        actionClickItem = actionClickItemParam;

        if (adsObj == null) {
            actionClickItem.runClick();

        } else {
            if (adsObj.getCurrentActivatedAds().equals(ApiClient.ADS_ADMOB)) {
                if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    actionClickItem.runClick();
                    generateInterstitialAd();
                }
            } else {
                startAppAd.loadAd(new AdEventListener() {
                    @Override
                    public void onReceiveAd(Ad ad) {
                    }

                    @Override
                    public void onFailedToReceiveAd(Ad ad) {
//                    actionClickItem.runClick();
                    }
                });

                StartAppAd.disableAutoInterstitial();
                startAppAd.showAd(new AdDisplayListener() {
                    @Override
                    public void adHidden(Ad ad) {
                        if (!runClickExecuted) {
                            runClickExecuted = true;
                            actionClickItem.runClick();
                        }
                        Log.e("YWV", "Action clicked");
                    }

                    @Override
                    public void adDisplayed(Ad ad) {
                    }

                    @Override
                    public void adClicked(Ad ad) {
                    }

                    @Override
                    public void adNotDisplayed(Ad ad) {
                    }
                });
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        runClickExecuted = false;
    }

    private void generateInterstitialAd() {
        mInterstitialAd = new InterstitialAd(getActivity());
        mInterstitialAd.setAdUnitId(adsObj.getADS_ADMOB_INTERSTITIALS());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
//                mNextLevelButton.setEnabled(true);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
//                mNextLevelButton.setEnabled(true);
            }

            @Override
            public void onAdClosed() {
                // Proceed to the next level.
                //finish();
                //showDialogApp();
                actionClickItem.runClick();

                generateInterstitialAd();
            }
        });
        AdRequest adRequestIn = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequestIn);
    }

    @Override
    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setQueryHint(getString(R.string.menu_search));
        searchView.setIconified(true);
        searchView.setOnQueryTextListener(this);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        skinAdapter.clear();
        query = s;
        loadFirstPage(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }
}