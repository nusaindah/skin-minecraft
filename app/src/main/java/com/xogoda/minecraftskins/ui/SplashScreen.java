package com.xogoda.minecraftskins.ui;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.xogoda.minecraftskins.R;
import com.xogoda.minecraftskins.ads.AdsAssistants;
import com.xogoda.minecraftskins.ads.AdsLoadListener;
import com.xogoda.minecraftskins.api.ApiClient;
import com.startapp.android.publish.ads.splash.SplashConfig;
import com.startapp.android.publish.adsCommon.StartAppAd;

/**
 * Created by KUNCORO on 16/10/2018.
 */

public class SplashScreen extends AppCompatActivity implements AdsLoadListener {

    private AdsAssistants adsAssistants;
    private Button btStart;
    private Bundle savedInstance;

    private LinearLayout linearProgress, linearRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);

        this.savedInstance = savedInstanceState;

        linearProgress = (LinearLayout) findViewById(R.id.linearProgress);
        linearRoot = (LinearLayout) findViewById(R.id.linearRoot);
        btStart = (Button) findViewById(R.id.btStart);

        linearProgress.setVisibility(View.VISIBLE);
        linearRoot.setVisibility(View.GONE);

        adsAssistants = new AdsAssistants(this);
        adsAssistants.setAdsLoadListener(this);
        adsAssistants.resetSession();
        adsAssistants.prepareAdsConfig();

    }

    @Override
    public void onConfigLoaded() {
        linearProgress.setVisibility(View.GONE);
        linearRoot.setVisibility(View.VISIBLE);

        if (ApiClient.ADS_CONFIGMODE == 1) {
            if (!adsAssistants.isFailedLoadOnlineConfig()) {
                //run normally check
                runActionAfterLoadAds();
            } else {
                //run thread splash
                showSplashWithThread();
            }
        } else { // mode ofline
            //run normally check
            runActionAfterLoadAds();
        }
    }

    private void runActionAfterLoadAds() {
        if (adsAssistants.getAdsMerge() == 1 || adsAssistants.getActivatedAds().equals(ApiClient.ADS_STARTAPP)) {
            showSplashAndButtonStart();
        } else {
            showSplashWithThread();
        }
    }

    private void showSplashAndButtonStart() {
        //start splash
        adsAssistants.runStartApp(false);
        StartAppAd.showSplash(this, savedInstance,
                new SplashConfig()
                        .setTheme(SplashConfig.Theme.OCEAN)
                        .setAppName(getString(R.string.app_name))
                        .setLogo(R.mipmap.ic_launcher)   // resource ID
                        .setOrientation(SplashConfig.Orientation.PORTRAIT)
        );

        btStart.setVisibility(View.VISIBLE);
        btStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    private void showSplashWithThread() {
        btStart.setVisibility(View.GONE);

        Thread splashThread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while (waited < 2000) {
                        sleep(100);
                        waited += 100;
                    }
                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    finish();

                }
            }
        };
        splashThread.start();
    }

}
