package com.xogoda.minecraftskins.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.xogoda.minecraftskins.R;
import com.xogoda.minecraftskins.api.ApiClient;
import com.xogoda.minecraftskins.api.ApiInterface;
import com.xogoda.minecraftskins.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrivacyPolicy extends AppCompatActivity {

    ProgressBar progressBar;
    LinearLayout layout_error;
    TextView txt_msg, txt_keterangan;
    Button btn_retry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_policy);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle(R.string.privacy_policy);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        layout_error = (LinearLayout) findViewById(R.id.layout_error);
        txt_msg = (TextView) findViewById(R.id.txt_msg);
        btn_retry = (Button) findViewById(R.id.btn_retry);
        txt_keterangan = (TextView) findViewById(R.id.txt_keterangan);

        progressBar.setVisibility(View.GONE);
        layout_error.setVisibility(View.GONE);

        btn_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_error.setVisibility(View.GONE);
                callData();
            }
        });

        if (Utilities.isConnected(getApplicationContext())) {
            callData();
        } else {
            layout_error.setVisibility(View.VISIBLE);
            txt_msg.setText(getString(R.string.no_internet_connection));
        }

    }

    private void callData() {
        progressBar.setVisibility(View.VISIBLE);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getSettings();
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jObj = new JSONObject(response.body().string());

                        String keterangan = jObj.getString("app_privacy_policy");

                        txt_keterangan.setText(fromHtml(keterangan));

                        progressBar.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    layout_error.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.failed_get_data));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                layout_error.setVisibility(View.VISIBLE);
                txt_msg.setText(t.getMessage());
            }
        });
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
