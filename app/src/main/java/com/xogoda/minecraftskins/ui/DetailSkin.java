package com.xogoda.minecraftskins.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.xogoda.minecraftskins.R;
import com.xogoda.minecraftskins.ads.AdsAssistants;
import com.xogoda.minecraftskins.ads.AdsObj;
import com.xogoda.minecraftskins.api.ApiClient;
import com.xogoda.minecraftskins.utils.ActionClickItem;
import com.xogoda.minecraftskins.utils.RandomString;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class DetailSkin extends AppCompatActivity {

    ImageView imageView;
    FloatingActionButton fab_save_gallery;

    String image_link;
    String image_dl;
    String title;
    int id;

    private final static int PERMISSION_REQUEST_CODE = 1;

    // Ads Online
    private AdsAssistants adsAssistants;
    private AdsObj adsObj;
    private ActionClickItem actionClickItem;
    private InterstitialAd mInterstitialAd;

    private StartAppAd startAppAd;
    private boolean runClickExecuted;

    private static final String TAG = DetailSkin.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_skin);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        MobileAds.initialize(this, String.valueOf(R.string.ads_app_id));

        imageView = (ImageView) findViewById(R.id.imageView);
        fab_save_gallery = (FloatingActionButton) findViewById(R.id.fab_item_save_gallery);

        Intent intent = getIntent();
        image_link = intent.getStringExtra("thumbnail");
        image_dl = intent.getStringExtra("url");
        title = intent.getStringExtra("title");
        id = intent.getIntExtra("id", 0);
        setTitle(title);

        Log.e(TAG, "id skin : " + id);

        Picasso.get()
                .load(image_link)
                .into(imageView);

        fab_save_gallery.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //TODO something when floating action menu second item clicked
                actionItemClicked(new ActionClickItem() {
                    @Override
                    public void runClick() {
                        if (checkPermission()) {
                            startImageDownload();
                        } else {
                            requestPermission();
                        }
                    }
                });
            }
        });

        adsAssistants = new AdsAssistants(this);
        adsObj = adsAssistants.getSessionAdsObj();
//        generateInterstitialAd();

        if (adsObj != null) {
            if (adsObj.getCurrentActivatedAds().equals(ApiClient.ADS_ADMOB)) {
                generateInterstitialAd();
            } else {
                startAppAd = new StartAppAd(this);
            }
        }

    }

    public void actionItemClicked(ActionClickItem actionClickItemParam) {
        actionClickItem = actionClickItemParam;

        if (adsObj == null) {
            actionClickItem.runClick();

        } else {
            if (adsObj.getCurrentActivatedAds().equals(ApiClient.ADS_ADMOB)) {
                if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    actionClickItem.runClick();
                    generateInterstitialAd();
                }
            } else {
                startAppAd.loadAd(new AdEventListener() {
                    @Override
                    public void onReceiveAd(Ad ad) {
                    }

                    @Override
                    public void onFailedToReceiveAd(Ad ad) {
//                    actionClickItem.runClick();
                    }
                });

                StartAppAd.disableAutoInterstitial();
                startAppAd.showAd(new AdDisplayListener() {
                    @Override
                    public void adHidden(Ad ad) {
                        if (!runClickExecuted) {
                            runClickExecuted = true;
                            actionClickItem.runClick();
                        }
                        Log.e("skin", "Action clicked");
                    }

                    @Override
                    public void adDisplayed(Ad ad) {
                    }

                    @Override
                    public void adClicked(Ad ad) {
                    }

                    @Override
                    public void adNotDisplayed(Ad ad) {
                    }
                });
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        runClickExecuted = false;
    }

    private void generateInterstitialAd() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(adsObj.getADS_ADMOB_INTERSTITIALS());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
//                mNextLevelButton.setEnabled(true);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
//                mNextLevelButton.setEnabled(true);
            }

            @Override
            public void onAdClosed() {
                // Proceed to the next level.
                //finish();
                //showDialogApp();

                actionClickItem.runClick();

                generateInterstitialAd();
            }
        });
        AdRequest adRequestIn = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequestIn);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void startImageDownload() {
        Picasso.get().load(image_dl).into(picassoImageTarget(RandomString.getAlphaNumericString(20) + ".png"));
    }

    private Target picassoImageTarget(final String imageName) {
        Log.e("saveImage", " picassoImageTarget");
        return new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                final File myImageFile = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DCIM), imageName); // Create image file
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(myImageFile);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                Log.e("saveImage", "image saved to >>>" + myImageFile.getAbsolutePath());
                Toast.makeText(getApplicationContext(), "Image Saved", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                Toast.makeText(getApplicationContext(), "Downloading image failed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                if (placeHolderDrawable != null) {
                }
                Toast.makeText(getApplicationContext(), "Downloading image", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    startImageDownload();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
