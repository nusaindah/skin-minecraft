package com.xogoda.minecraftskins.ui;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import com.xogoda.minecraftskins.R;
import com.xogoda.minecraftskins.ads.AdsAssistants;
import com.xogoda.minecraftskins.ads.AdsObj;
import com.xogoda.minecraftskins.api.ApiClient;
import com.google.android.gms.ads.MobileAds;
import com.startapp.android.publish.adsCommon.StartAppAd;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Dialog dialog;
    FragmentManager fragmentManager;
    Fragment fragment = null;
    Toolbar toolbar;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;

    // Ads Online
    private LinearLayout linearAds;
    private StartAppAd startAppAd;
    private AdsAssistants adsAssistants;
    private AdsObj adsObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        MobileAds.initialize(this, String.valueOf(R.string.ads_app_id));

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            navigationView.getMenu().getItem(0).setChecked(true);
            fragment = new Skin();
            callFragment(fragment);
        }
    }

    @Override
    public void onBackPressed() {
        drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            showDialogRateExit();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_skin) {
            fragment = new Skin();
            callFragment(fragment);
        } else if (id == R.id.nav_search) {
            fragment = new Search();
            callFragment(fragment);
        } else if (id == R.id.nav_privacy) {
            Intent intent = new Intent(this, PrivacyPolicy.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_give_rate) {
            rateApp();
        } else if (id == R.id.nav_exit) {
            showDialogRateExit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void callFragment(Fragment fragment) {
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container, fragment)
                .commit();
    }

    private void showDialogRateExit() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_rate);
        dialog.setTitle(getString(R.string.app_name));
        fullDialog(dialog);

        linearAds = (LinearLayout) findViewById(R.id.ad_linear);

        adsAssistants = new AdsAssistants(this);
        adsObj = adsAssistants.getSessionAdsObj();

        if (adsObj != null) {
            if (adsObj.getCurrentActivatedAds().equals(ApiClient.ADS_ADMOB)) {
                linearAds.setVisibility(View.VISIBLE);

                adsAssistants.createAdmobBanner(linearAds, adsObj.getADS_ADMOB_BANNER());
            } else {
                startAppAd = new StartAppAd(this);

                LinearLayout linearAds = (LinearLayout) findViewById(R.id.ad_linear);
                linearAds.setVisibility(View.VISIBLE);

                adsAssistants.createStartAppBanner(linearAds);
            }
        }

        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);
        Button btn_exit = (Button) dialog.findViewById(R.id.btn_exit);

        final int[] count = new int[1];
        final float[] curRate = new float[1];

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                // Decimal format for converting all floats into decimal
                DecimalFormat decimalFormat = new DecimalFormat("#");

                // Get Current rating
                curRate[0] = Float.valueOf(decimalFormat.format((curRate[0] * count[0] + rating) / ++count[0]));

                if (curRate[0] >= 3.0) {
                    rateApp();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.thanks_rate, Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });

        btn_exit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
                finish();
                System.exit(0);
            }
        });

        dialog.show();
    }

    private void fullDialog(Dialog dialog) {
        // Grab the window of the dialog_privacy, and change the width
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        layoutParams.copyFrom(window.getAttributes());
        // This makes the dialog_privacy take up the full width
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(layoutParams);
    }

    private void rateApp() {
        final String packageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.link_market) + packageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.link_play_store) + packageName)));
        }
    }

}
